package teamc.fem;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View.OnClickListener;

import teamc.fem.R;

/**
 * Created by J411 on 2015/07/13.
 */
public class Commentary extends Activity {

    int pos;
    String ans;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.commentary);

        Button nextPage = (Button) findViewById(R.id.nextPage); //”次の問題へ”ボタン
        Button topPage  = (Button) findViewById(R.id.examPage); //"問題画面に戻る"ボタン

        TextView title      = (TextView)findViewById(R.id.title);              //タイトルテキストフィールド
        TextView comment      = (TextView)findViewById(R.id.commentInfo);     //結果
        ImageView cmntIMG = (ImageView)findViewById(R.id.commentImage);  //解説図イメージ

        Intent intent = getIntent();
        String id = intent.getStringExtra("id");
        int res = intent.getIntExtra("result",-1);
        ans = intent.getStringExtra("answer");
        pos = intent.getIntExtra("pos",-1);

        title.setText(id.substring(id.length() - 2)+":解答");
        comment.setText(intent.getStringExtra("rans"));
        if(res == 0)cmntIMG.setImageResource(this.getResources().getIdentifier("bat", "drawable", "teamc.fem"));
        if(res == 1)cmntIMG.setImageResource(this.getResources().getIdentifier("right", "drawable", "teamc.fem"));

        topPage.setOnClickListener(new OnClickListener() { //問題選択画面に戻るボタンを押したと際の処理
            public void onClick(View v) {
                // Sub 画面を起動
                Intent intent = new Intent();
                intent.setClassName("teamc.fem", "teamc.fem.Question");
                intent.putExtra("res",0);
                intent.putExtra("pos",pos);
                intent.putExtra("answer",ans);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        nextPage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("res",1);
                intent.setClassName("teamc.fem", "teamc.fem.Question");
                intent.putExtra("pos",pos);
                intent.putExtra("answer",ans);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            Intent i2 = new Intent(this, MainActivity.class);
            startActivity(i2);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
