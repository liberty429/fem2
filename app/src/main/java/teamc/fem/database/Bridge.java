package teamc.fem.database;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import android.database.Cursor;
import android.util.Log;

/**
 * Created by j402 on 2015/07/06.
 */

//データベース操作のためのクラス
public class Bridge
{
    public static int CONTENT_TERM = 0;
    public static int CONTENT_QSN= 1;

    private SQLiteDatabase db;
    private Context context;

    //コンストラクタ
    public Bridge( Context context )
    {
        this.context = context;
        DBHelper dbhelper = new DBHelper( context );
        db = dbhelper.getWritableDatabase();
    }

    //ユーザー作成
    public boolean create_user( String name, String passwd ) throws Exception
    {
        db.execSQL("insert into user values(\""+ name +"\",\""+getMD5(passwd)+"\")");
        return true;
    }

    //ユーザー削除
    public boolean delete_user( String name, String passwd ) throws Exception
    {
        String[] cols = {"id"};
        String selection = "id = ? && passwd = ? ";
        String[] selectionArgs = { name, passwd };
        String groupBy = null;
        String having = null;
        String orderBy = null;
        Cursor cursor = db.query("user", cols, selection, selectionArgs, groupBy, having, orderBy);
        if( cursor.getCount() > 0 )
        {
            db.delete("table_name", "id = " + name, null);
            return true;
        }

        return false;
    }

    //ログイン
    public boolean login( String name, String passwd ) throws Exception
    {
        String[] cols = {"id","passwd"};
        String selection = "id = ?";
        String[] selectionArgs = { name };
        String groupBy = null;
        String having = null;
        String orderBy = null;
        Cursor cursor = db.query("user", cols, selection, selectionArgs, groupBy, having, orderBy);

        boolean next = cursor.moveToFirst();
        while (next) {
            //処理
           if( getMD5(passwd).equals(cursor.getString(1)) )
           {
                return true;
           }
            // 次のレコード
            next = cursor.moveToNext();
        }
        cursor.close();
        return false;
    }

    //DBを閉じる
    public void close() throws Exception
    {
        db.close();
    }

    public void insertHis(String user, String id, int result)
    {
        db.execSQL("delete from history where user_id = '"+user+"' & question_id = '"+id+"';");
        db.execSQL("insert into history values('"+user+"','"+id+"',"+result+");");
        return;
    }

    public int getHis(String user, String id)
    {
        String[] cols = {"result"};
        String selection = "user_id = ? & question_id = ?";
        String[] selectionArgs = { user,id };
        String groupBy = null;
        String having = null;
        String orderBy = null;
        Cursor cursor = db.query("history", cols, selection, selectionArgs, groupBy, having, orderBy);
        boolean next = cursor.moveToFirst();
        while (next) {
            //処理
            return cursor.getInt(2);
        }
        return -1;
    }

    //ハッシュ値を求める
    public String getMD5( String c ) throws Exception
    {
        MessageDigest md = MessageDigest.getInstance("MD5");
        // ハッシュ値を計算
        md.update(c.getBytes());
        byte[] digest = md.digest();

        // 16進数文字列に変換
        StringBuffer buffer = new StringBuffer();
        for(int i = 0; i < digest.length; i++) {
            String tmp = Integer.toHexString(digest[i] & 0xff);
            if (tmp.length() == 1) {
                buffer.append('0').append(tmp);
            } else {
                buffer.append(tmp);
            }
        }
        System.out.println(buffer.toString());
        return buffer.toString();
    }

    //検索
    public Cursor getQuery()
    {
        String sql = "select * from question;";
        Cursor cursor = db.rawQuery(sql,null);
        return cursor;
    }


}
