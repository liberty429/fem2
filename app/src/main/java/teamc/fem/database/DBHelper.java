package teamc.fem.database;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by j402 on 2015/07/06.
 */
public class DBHelper extends SQLiteOpenHelper
{
    private final static String DB_NAME = "fem.db";
    private final static int DB_VERSION = 1;
    private Context context;

    //コンストラクタ
    public DBHelper( Context context)
    {
        super( context, DB_NAME, null, DB_VERSION ); this.context = context;
    }

    //データベースの生成
    @Override
    public void onCreate( SQLiteDatabase db )
    {
        db.execSQL( "create table if not exists user    ( id text primary key, passwd text )" );
        db.execSQL( "create table if not exists question( id text primary key," +
                " contents text, image num, option1 text, option2 text," +
                " option3 text, option4 text, answer num )" );
        db.execSQL( "create table if not exists term    ( id text primary key, year integer, term integer )" );
        db.execSQL( "create table if not exists history ( user_id text, question_id text, result )" );
        setDefault(db);
    }

    //データベースのアップグレード
    @Override
    public void onUpgrade( SQLiteDatabase db, int aldVersion, int newVersion )
    {
        db.execSQL( "drop table if not exists user" );
        db.execSQL( "drop table if not exists question" );
        db.execSQL( "drop table if not exists term" );
        db.execSQL( "drop table if not exists history" );
        onCreate( db );
    }

    public void setDefault(SQLiteDatabase db){
        try {
            execSql( db, "sql" );

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void execSql(SQLiteDatabase db,String assetsDir) throws IOException {
        AssetManager as = context.getResources().getAssets();
        try {
            String files[] = as.list(assetsDir);
            for (int i = 0; i < files.length; i++) {
                String str = readFile(as.open(assetsDir + "/" + files[i]));
                Log.e("1",str);
                String st[] = str.split("#");
                Log.e("2",""+st.length);
                for (String sql: st){
                    db.execSQL(sql+";");
                    Log.e("3",sql+";");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String readFile(InputStream is) throws IOException{
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(is,"UTF-8"));

            StringBuilder sb = new StringBuilder();
            String str;
            while((str = br.readLine()) != null){
                sb.append(str);
            }
            return sb.toString();

        } finally {
            if (br != null) br.close();
        }
    }
}
