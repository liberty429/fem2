package teamc.fem;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import teamc.fem.database.Bridge;



public class Signup extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);

        final Bridge db = new Bridge(this);

        Button login = (Button)findViewById(R.id.sign_up);

        login.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                try {
                    EditText id_text = (EditText) findViewById(R.id.id_text);
                    EditText pass_text = (EditText) findViewById(R.id.pass_text);
                    SpannableStringBuilder id_sp = (SpannableStringBuilder) id_text.getText();
                    SpannableStringBuilder pass_sp = (SpannableStringBuilder) pass_text.getText();

                    if ( db.create_user(id_sp.toString(), pass_sp.toString())) {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(Signup.this);

                        // ダイアログの設定
                        alertDialog.setTitle("Success");      //タイトル設定
                        alertDialog.setMessage("ユーザ: "+id_sp.toString()+"\nパスワード: "+pass_sp.toString());  //内容(メッセージ)設定

                        // OK(肯定的な)ボタンの設定
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // OKボタン押下時の処理
                                Intent intent = new Intent();
                                intent.setClassName("teamc.fem", "teamc.fem.MainActivity");
                                startActivity(intent);
                            }
                        });

                        alertDialog.show();
                    }
                } catch (Exception e) {
                }
            }
        });
    }

}
