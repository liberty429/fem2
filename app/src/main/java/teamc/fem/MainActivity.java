package teamc.fem;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import teamc.fem.database.Bridge;



public class MainActivity extends Activity {

    Bridge db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        db = new Bridge(this);

        Button login = (Button)findViewById(R.id.login_button);
        Button sign = (Button)findViewById(R.id.sign_button);
        login.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                try {
                    EditText id_text = (EditText) findViewById(R.id.id_text);
                    EditText pass_text = (EditText) findViewById(R.id.pass_text);
                    SpannableStringBuilder id_sp = (SpannableStringBuilder) id_text.getText();
                    SpannableStringBuilder pass_sp = (SpannableStringBuilder) pass_text.getText();

                    if (db.login(id_sp.toString(), pass_sp.toString())) {
                        // Question 画面を起動
                        Intent intent = new Intent();
                        intent.setClassName("teamc.fem", "teamc.fem.Question");
                        intent.putExtra("user",id_sp.toString());
                        startActivity(intent);

                    }else showDialog("ユーザー・パスワードが間違っています");
                } catch (Exception e) {
                    showDialog("エラー");
                }
            }
        });

        sign.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClassName("teamc.fem", "teamc.fem.Signup");
                startActivity(intent);
            }
        });
    }

    public void showDialog( String s ){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

        // ダイアログの設定
        alertDialog.setTitle("エラー");      //タイトル設定
        alertDialog.setMessage(s);  //内容(メッセージ)設定

        // OK(肯定的な)ボタンの設定
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // OKボタン押下時の処理
            }
        });

        alertDialog.show();
    }

}
