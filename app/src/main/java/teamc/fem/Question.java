package teamc.fem;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import teamc.fem.database.Bridge;

public class Question extends Activity implements View.OnClickListener {

    private static final int SUB_ACTIVITY = 1001;

    RadioGroup rg;
    Cursor cr;

    String q_ansP=" ";//プレイヤーの答え
    String q_id;

    Button next;
    TextView nu;
    TextView qu;
    ImageView im1;
    ImageView im;
    ImageView ima;
    ImageView imi;
    ImageView imu;
    ImageView ime;
    RadioButton a;
    RadioButton i;
    RadioButton u;
    RadioButton e;
    Bridge db;
    String user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question);
        Intent intent = getIntent();
        user = intent.getStringExtra("user");

        next = (Button)findViewById(R.id.q_next);
        nu = (TextView)findViewById(R.id.q_number);
        qu = (TextView)findViewById(R.id.q_question);
        im1 = (ImageView)findViewById(R.id.q_image1);
        im = (ImageView)findViewById(R.id.q_image);
        ima = (ImageView)findViewById(R.id.q_imageA);
        imi = (ImageView)findViewById(R.id.q_imageI);
        imu = (ImageView)findViewById(R.id.q_imageU);
        ime = (ImageView)findViewById(R.id.q_imageE);
        a= (RadioButton)findViewById(R.id.a);
        i= (RadioButton)findViewById(R.id.i);
        u= (RadioButton)findViewById(R.id.u);
        e= (RadioButton)findViewById(R.id.e);

        db = new Bridge(this);
        cr = db.getQuery();
        Log.e("",""+cr.getCount());
        if( cr.getCount() > 0 ) {
            cr.moveToFirst();
            Log.e("",""+db.getHis(user,cr.getString(0)));
            while(!cr.isBeforeFirst())
            {
                if(db.getHis(user,cr.getString(0)) == 1)cr.moveToNext();
                else break;
            }
            nu.setText(cr.getString(0));
            q_id = cr.getString(0);
            qu.setText(cr.getString(1));
            a.setText("ア");
            i.setText("イ");
            u.setText("ウ");
            e.setText("エ");
            if(cr.getString(3) != null)a.setText(cr.getString(3));
            if(cr.getString(4) != null)i.setText(cr.getString(4));
            if(cr.getString(5) != null)u.setText(cr.getString(5));
            if(cr.getString(6) != null)e.setText(cr.getString(6));
        }

        im1.setImageResource(this.getResources().getIdentifier("f"+cr.getString(0).toLowerCase()+"m", "drawable", "teamc.fem"));
        im.setImageResource(this.getResources().getIdentifier("f"+cr.getString(0).toLowerCase()+"a", "drawable", "teamc.fem"));
        ima.setImageResource(this.getResources().getIdentifier("f"+cr.getString(0).toLowerCase()+"a1", "drawable", "teamc.fem"));
        imi.setImageResource(this.getResources().getIdentifier("f"+cr.getString(0).toLowerCase()+"a2", "drawable", "teamc.fem"));
        imu.setImageResource(this.getResources().getIdentifier("f"+cr.getString(0).toLowerCase()+"a3", "drawable", "teamc.fem"));
        ime.setImageResource(this.getResources().getIdentifier("f"+cr.getString(0).toLowerCase()+"a4", "drawable", "teamc.fem"));

        rg =(RadioGroup)findViewById(R.id.rg);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) findViewById(checkedId);
                q_ansP = (String)radioButton.getText();
            }
        });

        next.setOnClickListener(this);
        rg.check(a.getId());
        q_ansP = a.getText().toString();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.q_next:
                int res = 0;
                Intent i1 = new Intent(this,Commentary.class);
                i1.putExtra("id", q_id);
                String a = "";
                switch(cr.getInt(7))
                {
                    case 1:
                        a = cr.getString(3);
                        if(cr.getString(3) == null) a = "ア";
                        break;
                    case 2:
                        a = cr.getString(4);
                        if(cr.getString(4) == null) a = "イ";
                        break;
                    case 3:
                        a = cr.getString(5);
                        if(cr.getString(5) == null) a = "ウ";
                        break;
                    case 4:
                        a = cr.getString(6);
                        if(cr.getString(6) == null) a = "エ";
                        break;
                }
                if(q_ansP.equals(a)) res = 1;

                if(res == 1)db.insertHis(user,q_id,1);
                else db.insertHis(user,q_id,0);

                i1.putExtra("answer", q_ansP);
                i1.putExtra("rans", a);
                i1.putExtra("result", res);
                i1.putExtra("pos", cr.getPosition());
                startActivityForResult(i1, SUB_ACTIVITY);
                break;

            case R.id.rg:

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SUB_ACTIVITY) {

            if (resultCode == RESULT_OK) {

                if(data.getIntExtra("res",-1) == 0) {
                    int pos = data.getIntExtra("pos", -1);
                    if (pos != -1) cr.moveToPosition(pos);
                    if( cr.getCount() > 0 ) {

                        nu.setText(cr.getString(0));
                        q_id = cr.getString(0);
                        qu.setText(cr.getString(1));
                        a.setText("ア");
                        i.setText("イ");
                        u.setText("ウ");
                        e.setText("エ");
                        if(cr.getString(3) != null)a.setText(cr.getString(3));
                        if(cr.getString(4) != null)i.setText(cr.getString(4));
                        if(cr.getString(5) != null)u.setText(cr.getString(5));
                        if(cr.getString(6) != null)e.setText(cr.getString(6));
                    }

                    im1.setImageResource(this.getResources().getIdentifier("f"+cr.getString(0).toLowerCase()+"m", "drawable", "teamc.fem"));
                    im.setImageResource(this.getResources().getIdentifier("f"+cr.getString(0).toLowerCase()+"a", "drawable", "teamc.fem"));
                    ima.setImageResource(this.getResources().getIdentifier("f"+cr.getString(0).toLowerCase()+"a1", "drawable", "teamc.fem"));
                    imi.setImageResource(this.getResources().getIdentifier("f"+cr.getString(0).toLowerCase()+"a2", "drawable", "teamc.fem"));
                    imu.setImageResource(this.getResources().getIdentifier("f"+cr.getString(0).toLowerCase()+"a3", "drawable", "teamc.fem"));
                    ime.setImageResource(this.getResources().getIdentifier("f"+cr.getString(0).toLowerCase()+"a4", "drawable", "teamc.fem"));



                    String rt = data.getStringExtra("answer");
                    String at = a.getText().toString();
                    String it = i.getText().toString();
                    String ut = u.getText().toString();
                    String et = e.getText().toString();

                    if(rt.equals(at)) a.setSelected(true);
                    if(rt.equals(it)) i.setSelected(true);
                    if(rt.equals(ut)) u.setSelected(true);
                    if(rt.equals(et)) e.setSelected(true);

                }
                if(data.getIntExtra("res",-1) == 1) {

                    int pos = data.getIntExtra("pos", -1);
                    if (pos != -1) cr.moveToPosition(pos);

                    cr.moveToNext();
                    if (cr.getCount() > 0) {
                        while (!cr.isBeforeFirst()) {
                            if (db.getHis(user, cr.getString(0)) == 1) cr.moveToNext();
                            else break;
                        }

                        nu.setText(cr.getString(0));
                        q_id = cr.getString(0);
                        qu.setText(cr.getString(1));
                        a.setText("ア");
                        i.setText("イ");
                        u.setText("ウ");
                        e.setText("エ");
                        if (cr.getString(3) != null) a.setText(cr.getString(3));
                        if (cr.getString(4) != null) i.setText(cr.getString(4));
                        if (cr.getString(5) != null) u.setText(cr.getString(5));
                        if (cr.getString(6) != null) e.setText(cr.getString(6));
                    }

                    im1.setImageResource(this.getResources().getIdentifier("f" + cr.getString(0).toLowerCase() + "m", "drawable", "teamc.fem"));
                    im.setImageResource(this.getResources().getIdentifier("f" + cr.getString(0).toLowerCase() + "a", "drawable", "teamc.fem"));
                    ima.setImageResource(this.getResources().getIdentifier("f" + cr.getString(0).toLowerCase() + "a1", "drawable", "teamc.fem"));
                    imi.setImageResource(this.getResources().getIdentifier("f" + cr.getString(0).toLowerCase() + "a2", "drawable", "teamc.fem"));
                    imu.setImageResource(this.getResources().getIdentifier("f" + cr.getString(0).toLowerCase() + "a3", "drawable", "teamc.fem"));
                    ime.setImageResource(this.getResources().getIdentifier("f" + cr.getString(0).toLowerCase() + "a4", "drawable", "teamc.fem"));

                    rg.check(a.getId());
                    q_ansP = a.getText().toString();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            Intent i2 = new Intent(this, MainActivity.class);
            startActivity(i2);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}